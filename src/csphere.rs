use std::f32::consts::PI;
use glam::{vec2, Vec4, vec4};
use crate::traits::Geometry;

/// Sphere geometry. Based off of Cinder's geometry, but missing tangent support
/// https://github.com/cinder/Cinder/blob/master/src/cinder/GeomIo.cpp#L1994
pub struct Sphere {}

impl Sphere {
    pub fn create_with_size(radius: f32, subdivisions: i32) -> Geometry {
        let center = Vec4::new(0.0, 0.0, 0.0, 0.0);

        let rings_segments = Sphere::numRingsAndSegments(subdivisions, radius);
        let num_rings = rings_segments[0];
        let num_segments = rings_segments[1];


        let ring_inc = 1.0 / ((num_rings - 1) as f32);
        let seg_inc = 1.0 / ((num_segments - 1) as f32);

        let mut positions = vec![];
        let mut normals = vec![];
        let mut uvs = vec![];

        for r in 0..num_rings {
            let v = (r as f32) * ring_inc;
            for s in 0..num_segments {
                let u = 1.0 - (s as f32) * seg_inc;

                let x = ((PI * 2.0) * u).sin() * (PI * v).sin();
                let y = (PI * (v - 0.5)).sin();
                let z = ((PI * 2.0) * u).cos() * (PI * v).sin();

                positions.push(vec4(
                    x * radius + center.x,
                    y * radius + center.y,
                    z * radius + center.z,
                    1.0,
                ));

                normals.push(vec4(x, y, z, 1.0));

                uvs.push(vec2(u, v));
            }
        }


        let mut indices = vec![];
        for r in 0..num_rings {
            for s in 0..num_segments {
                indices.push((r * num_segments + (s + 1)) as u32);
                indices.push((r * num_segments + s) as u32);
                indices.push(((r + 1) * num_segments + (s + 1)) as u32);

                indices.push(((r + 1) * num_segments + s) as u32);
                indices.push(((r + 1) * num_segments + (s + 1)) as u32);
                indices.push((r * num_segments + s) as u32);
            }
        }

        Geometry {
            positions,
            normals,
            uvs,
            indices,
        }
    }

    pub fn create() -> Geometry {
        let subdivisions = 18;
        let center = Vec4::new(0.0, 0.0, 0.0, 0.0);
        let radius = 1.0;

        let rings_segments = Sphere::numRingsAndSegments(subdivisions, radius);
        let num_rings = rings_segments[0];
        let num_segments = rings_segments[1];


        let ring_inc = 1.0 / ((num_rings - 1) as f32);
        let seg_inc = 1.0 / ((num_segments - 1) as f32);

        let mut positions = vec![];
        let mut normals = vec![];
        let mut uvs = vec![];

        for r in 0..num_rings {
            let v = (r as f32) * ring_inc;
            for s in 0..num_segments {
                let u = 1.0 - (s as f32) * seg_inc;

                let x = ((PI * 2.0) * u).sin() * (PI * v).sin();
                let y = (PI * (v - 0.5)).sin();
                let z = ((PI * 2.0) * u).cos() * (PI * v).sin();

                positions.push(vec4(
                    x * radius + center.x,
                    y * radius + center.y,
                    z * radius + center.z,
                    1.0,
                ));

                normals.push(vec4(x, y, z, 1.0));

                uvs.push(vec2(u, v));
            }
        }


        let mut indices = vec![];
        for r in 0..num_rings {
            for s in 0..num_segments {
                indices.push((r * num_segments + (s + 1)) as u32);
                indices.push((r * num_segments + s) as u32);
                indices.push(((r + 1) * num_segments + (s + 1)) as u32);

                indices.push(((r + 1) * num_segments + s) as u32);
                indices.push(((r + 1) * num_segments + (s + 1)) as u32);
                indices.push((r * num_segments + s) as u32);
            }
        }

        Geometry {
            positions,
            normals,
            uvs,
            indices,
        }
    }


    fn numRingsAndSegments(subdivisions: i32, radius: f32) -> [i32; 2] {
        let mut num_segments = subdivisions.clone();

        if num_segments < 4 {
            num_segments = 12.max((radius * (PI * 2.0)) as i32);
        }

        let num_rings = (num_segments >> 1) + 1;
        num_segments += 1;


        [num_rings, num_segments]
    }
}