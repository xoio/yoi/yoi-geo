use glam::{Vec2, vec2, Vec4, vec4};
use crate::traits::{Geometry};

/// A port of @vorg's primitive-cube package
/// https://github.com/vorg/primitive-cube/
pub struct CubeGeo {}

impl CubeGeo {
    pub fn create(sx: f32, sy: f32, sz: f32, nx: f32, ny: f32, nz: f32) -> Geometry {

        let mut positions = vec![];
        let mut normals = vec![];
        let mut uvs = vec![];
        let mut cells = vec![];

        let mut vertex_index: usize = 0;

        let mut make_plane = |u: usize, v: usize, w: usize, su: f32, sv: f32, nu: usize, nv: usize, pw: f32, flipu: f32, flipv: f32| {
            let vert_shift = vertex_index;

            for j in 0..nv + 1 {
                for i in 0..nu + 1  {

                    let mut p = Vec4::new(0.0,0.0,0.0,1.0);
                    p[u] = (-su / 2.0 + (i as f32) * su / (nu as f32)) * flipu;
                    p[v] = (-sv / 2.0 + (j as f32) * sv / (nv as f32)) * flipv;
                    p[w] = pw;
                    positions.push(p);

                    let mut n = Vec4::new(0.0,0.0,0.0,1.0);
                    n[u] = 0.0f32;
                    n[v] = 0.0f32;
                    n[w] = pw / pw.abs();
                    normals.push(n);

                    let u = (i / nu) as f32;
                    let v = 1.0 / ((j / nv) as f32);
                    uvs.push(Vec2::new(u, v));

                    vertex_index += 1;
                }
            }

            for j in 0..nv {
                for i in 0..nu {
                    let n = vert_shift + j * (nu + 1) + i;
                    cells.push([n, n + nu  + 1, n + nu + 2]);
                    cells.push([n, n + nu + 2, n + 1]);
                }
            }
        };




        make_plane(0, 1, 2, sx, sy, nx as usize, ny as usize, sz / 2.0, 1.0, -1.0); //front
        make_plane(0, 1, 2, sx, sy, nx as usize, ny as usize, -sz / 2.0, -1.0, -1.0); //back
        make_plane(2, 1, 0, sz, sy, nz as usize, ny as usize, -sx / 2.0, 1.0, -1.0); //left
        make_plane(2, 1, 0, sz, sy, nz as usize, ny as usize, sx / 2.0, -1.0, -1.0); //right
        make_plane(0, 2, 1, sx, sz, nx as usize, nz as usize, sy / 2.0, 1.0, 1.0); //top
        make_plane(0, 2, 1, sx, sz, nx as usize, nz as usize, -sy / 2.0, 1.0, -1.0); //bottom


        let mut indices = vec![];

        for cell in cells.iter() {

            indices.push(cell[0] as u32);
            indices.push(cell[1] as u32);
            indices.push(cell[2] as u32);
        }

        Geometry {
            positions,
            normals,
            uvs,
            indices
        }
    }
}
/// Ported from work of Robert Hodgin(@flight404)
/// https://github.com/flight404/Eyeo2012
pub struct BoxRoom {

}

impl BoxRoom {
    pub fn new()->Self {
        let x = 1.0 as f32;
        let y = 1.0 as f32;
        let z = 1.0 as f32;

        let verts = vec![
            vec4(-x,-y,-z,1.0 ), vec4(-x,-y, z,1.0 ),
            vec4( x,-y, z,1.0 ), vec4( x,-y,-z,1.0 ),
            vec4(-x, y,-z,1.0 ), vec4(-x, y, z,1.0 ),
            vec4( x, y, z,1.0 ), vec4( x, y,-z,1.0 )
        ];

        let v_indices = vec![
            [0,1,3], [1,2,3],	// floor
            [4,7,5], [7,6,5],	// ceiling
            [0,4,1], [4,5,1],	// left
            [2,6,3], [6,7,3],	// right
            [1,5,2], [5,6,2],	// back
            [3,7,0], [7,4,0]  // front
        ];

        let v_normals = vec![
            vec4( 0.0, 1.0, 0.0, 1.0 ),	// floor
            vec4( 0.0,-1.0, 0.0, 1.0 ),	// ceiling
            vec4( 1.0, 0.0, 0.0, 1.0 ),	// left
            vec4(-1.0, 0.0, 0.0, 1.0 ),	// right
            vec4( 0.0, 0.0,-1.0, 1.0 ),	// back
            vec4( 0.0, 0.0, 1.0, 1.0 ) 	// front
        ];

        let v_tex_coords = vec![
            vec2( 0.0, 0.0),
            vec2( 0.0, 1.0),
            vec2( 1.0, 1.0),
            vec2( 1.0, 0.0)
        ];

        let t_indices = vec![
            [0,1,3], [1,2,3],	// floor
            [0,1,3], [1,2,3],	// ceiling
            [0,1,3], [1,2,3],	// left
            [0,1,3], [1,2,3],	// right
            [0,1,3], [1,2,3],	// back
            [0,1,3], [1,2,3] 	// front
        ];

        let mut positions = vec![];
        let mut indices = vec![];
        let mut normals = vec![];
        let mut uvs = vec![];

        #[allow(unused)]
        let mut index = 0;
        for i in 0..12 {
            positions.push(verts[v_indices[i][0]]);
            positions.push(verts[v_indices[i][1]]);
            positions.push(verts[v_indices[i][2]]);

            indices.push(index+=1 as u32);
            indices.push(index+=1 as u32);
            indices.push(index+=1 as u32);

            normals.push(v_normals[i / 2]);
            normals.push(v_normals[i / 2]);
            normals.push(v_normals[i / 2]);

            uvs.push(v_tex_coords[t_indices[i][0]]);
            uvs.push(v_tex_coords[t_indices[i][1]]);
            uvs.push(v_tex_coords[t_indices[i][2]]);
        }


        BoxRoom{}
    }
}