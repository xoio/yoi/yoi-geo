use glam::{vec4, Vec4};

/// Builds a DeQuan strange attractor.
pub struct DeQuan {
    a: f32,
    b: f32,
    c: f32,
    d: f32,
    e: f32,
    f: f32,
    x: f32,
    y: f32,
    z: f32,
    num_points: u32,
    positions: Vec<Vec4>,
}

impl DeQuan {
    pub fn new(num_points: u32) -> Self {
        DeQuan {
            a: 40.0,
            b: 1.833,
            c: 0.16,
            d: 0.6,
            e: 55.0,
            f: 19.3,
            x: 2.5,
            y: 3.2,
            z: 0.4,
            num_points,
            positions: vec![],
        }
    }

    pub fn get_points(&self) -> Vec<Vec4> {
        self.positions.clone()
    }

    pub fn build_points(&mut self) {
        let dt = 0.0002;
        let scale = 1.0;
        let mut points = vec![];

        #[allow(unused)]
        for i in 0..self.num_points {
            let mut x1 = self.a * (self.y - self.x) + self.c * self.x * self.z;
            let mut y1 = self.e * self.x + self.f * self.y - self.x * self.z;
            let mut z1 = self.b * self.z + self.x * self.y - self.d * self.x * self.x;

            x1 *= dt;
            y1 *= dt;
            z1 *= dt;

            self.x += x1;
            self.y += y1;
            self.z += z1;

            points.push(vec4(self.x * scale, self.y * scale, self.z * scale, 1.));
        }


        self.positions = points;
    }
}