use glam::{Vec2, Vec3, vec3, Vec4, vec4};
use crate::traits::{Geometry};

/// port of Icosahedron geo from Cinder.
/// https://github.com/cinder/Cinder/blob/master/src/cinder/GeomIo.cpp#L1234
pub struct IcosahedronGeo {
    pub positions: Vec<Vec4>,
    pub normals: Vec<Vec4>,
    pub uvs: Vec<Vec2>,
    pub indices: Vec<u32>,
}

impl IcosahedronGeo {
    pub fn create() -> Geometry {
        #[allow(non_snake_case)]
            let PHI: f32 = 1.0 / ((1.0 + (5.0_f32.sqrt() as f32)) / 2.0);

        #[allow(non_snake_case)]
            let S_POSITIONS: [f32; 36] = [
            -PHI, 1.0, 0.0, PHI, 1.0, 0.0, -PHI, -1.0, 0.0, PHI, -1.0, 0.0,
            0.0, -PHI, 1.0, 0.0, PHI, 1.0, 0.0, -PHI, -1.0, 0.0, PHI, -1.0,
            1.0, 0.0, -PHI, 1.0, 0.0, PHI, -1.0, 0.0, -PHI, -1.0, 0.0, PHI
        ];


        /*
                let S_TEX_COORDS:[f32;120] = [
            0.0, 0.0, 0.0, 1.0, 1.0, 0.0,
            0.0, 0.0, 0.0, 1.0, 1.0, 0.0,
            0.0, 0.0, 0.0, 1.0, 1.0, 0.0,
            0.0, 0.0, 0.0, 1.0, 1.0, 0.0,
            0.0, 0.0, 0.0, 1.0, 1.0, 0.0,

            1.0, 0.0, 0.0, 1.0, 1.0, 1.0,
            1.0, 0.0, 0.0, 1.0, 1.0, 1.0,
            1.0, 0.0, 0.0, 1.0, 1.0, 1.0,
            1.0, 0.0, 0.0, 1.0, 1.0, 1.0,
            1.0, 0.0, 0.0, 1.0, 1.0, 1.0,

            1.0, 1.0, 0.0, 1.0, 1.0, 0.0,
            1.0, 1.0, 0.0, 1.0, 1.0, 0.0,
            1.0, 1.0, 0.0, 1.0, 1.0, 0.0,
            1.0, 1.0, 0.0, 1.0, 1.0, 0.0,
            1.0, 1.0, 0.0, 1.0, 1.0, 0.0,

            1.0, 0.0, 0.0, 1.0, 0.0, 0.0,
            1.0, 0.0, 0.0, 1.0, 0.0, 0.0,
            1.0, 0.0, 0.0, 1.0, 0.0, 0.0,
            1.0, 0.0, 0.0, 1.0, 0.0, 0.0,
            1.0, 0.0, 0.0, 1.0, 0.0, 0.0
        ];

         */
        const S_INDICES: [u32; 60] = [
            0, 11, 5, 0, 5, 1, 0, 1, 7, 0, 7, 10, 0, 10, 11,
            5, 11, 4, 1, 5, 9, 7, 1, 8, 10, 7, 6, 11, 10, 2,
            3, 9, 4, 3, 4, 2, 3, 2, 6, 3, 6, 8, 3, 8, 9,
            4, 9, 5, 2, 4, 11, 6, 2, 10, 8, 6, 7, 9, 8, 1
        ];

        let mut tmp_pos = vec![];
        let mut indices = vec![];
        let mut normals = vec![];
        normals.resize(60, Vec3::new(0.0, 0.0, 0.0));

        //let mut normals = [];

        // get positions.
        for i in 0..60 {
            let idx = (S_INDICES[i] * 3) as usize;

            let x = S_POSITIONS[idx];
            let y = S_POSITIONS[idx + 1];
            let z = S_POSITIONS[idx + 2];

            tmp_pos.push(vec3(x, y, z))
        }

        // build indices
        for i in 0..60 {
            indices.push(i as u32)
        }

        // build normals
        let num_triangles = indices.len() / 3;

        for i in 0..num_triangles {
            let index0 = indices[i * 3] as usize;
            let index1 = indices[i * 3 + 1] as usize;
            let index2 = indices[i * 3 + 2] as usize;

            let v0 = tmp_pos[index0];
            let v1 = tmp_pos[index1];
            let v2 = tmp_pos[index2];

            let e0 = v1 - v0;
            let e1 = v2 - v0;

            normals[index0] = e0.cross(e1);
            normals[index1] = e0.cross(e1);
            normals[index2] = e0.cross(e1);
        }

        let mut final_pos = vec![];
        let mut final_normals = vec![];

        //// Package into Vec4 for easier portability ////
        for pos in tmp_pos {
            final_pos.push(
                vec4(
                    pos.x,
                    pos.y,
                    pos.z,
                    1.0,
                )
            )
        }

        for norm in normals {
            final_normals.push(
                vec4(
                    norm.x,
                    norm.y,
                    norm.z,
                    1.0,
                )
            )
        }

        Geometry {
            positions: final_pos,
            normals: final_normals,
            uvs: vec![], // TODO add uvs
            indices,
        }
    }
}

