use glam::{vec2, vec4};
use gltf::image::Data;
use crate::traits::Geometry;

pub struct GLTFGeometry {
    pub geo: Geometry,
    images: Vec<Data>,
}

/// General loading function. Loads mesh geometry into a [Geometry] struct.
pub fn load_gltf_model(path: &str) -> GLTFGeometry {
    let mut positions = vec![];
    let mut normals = vec![];
    let mut uvs = vec![];
    let mut indices = vec![];

    let (gltf, buffers, images) = gltf::import(path).unwrap();

    for mesh in gltf.meshes() {
        for prim in mesh.primitives() {
            let reader = prim.reader(|buffer| {
                Some(&buffers[buffer.index()])
            });

            // Read positions.
            if let Some(iter) = reader.read_positions() {
                for pos in iter {
                    positions.push(vec4(pos[0], pos[1], pos[2], 1.))
                }
            }

            // Read normals
            if let Some(iter) = reader.read_normals() {
                for norm in iter {
                    normals.push(vec4(norm[0], norm[1], norm[2], 1.))
                }
            }

            // Read uvs
            // assumes one set of texture coordinates.
            if let Some(iter) = reader.read_tex_coords(0) {
                for uv in iter.into_f32() {
                    uvs.push(vec2(uv[0], uv[1]))
                }
            }

            // Read indices
            if let Some(iter) = reader.read_indices() {
                for idx in iter.into_u32() {
                    indices.push(idx);
                }
            }
        }
    }


    GLTFGeometry {
        geo: Geometry {
            positions,
            normals,
            uvs,
            indices,
        },
        images
    }
}