use tobj;
use std::fs;
use glam::{vec2, vec4};
use crate::traits::{Geometry};

/// OBJ helpers using https://github.com/Twinklebear/tobj

/// Loads obj model. Parses values out to Vec objects of data.
/// Currently does simplified loading, see tobj crate for more examples.
pub fn load_model(filepath: &str) -> Vec<Geometry> {
    let s = format!("Something went wrong trying to load .obj model at {}", filepath);
    let model = fs::read_to_string(filepath).expect(s.as_str());

    let mut geos = vec![];

    let (models, materials) = tobj::load_obj(
        &model.as_str(),
        &tobj::LoadOptions::default(),
    ).expect("failed to parse obj");

    for (i, m) in models.iter().enumerate() {
        let mesh = &m.mesh;

        if cfg!(debug_assertions) {
            println!("");
            println!("model[{}].name             = \'{}\'", i, m.name);
            println!("model[{}].mesh.material_id = {:?}", i, mesh.material_id);
            println!(
                "model[{}].face_count       = {}",
                i,
                mesh.face_arities.len()
            );
        }


        ////// BUILD UVS AND INDICES ///////////

        let mut indices = vec![];
        let mut _uvs = vec![];

        let mut next_face = 0;
        for face in 0..mesh.face_arities.len() {
            let end = next_face + mesh.face_arities[face] as usize;
            let face_indices = &mesh.indices[next_face..end];

            indices = face_indices.to_vec();

            if !mesh.texcoord_indices.is_empty() {
                let texcoord_face_indices = &mesh.texcoord_indices[next_face..end];
                _uvs = texcoord_face_indices.to_vec();
            }
        }

        // package uvs
        let mut uvs = vec![];
        for i in 0.._uvs.len() {
            uvs.push(
                vec2(_uvs[i] as f32, _uvs[i + 1] as f32)
            )
        }


        ///////// BUILD POSITIONS ///////////
        let mut positions = vec![];
        for vtx in 0..mesh.positions.len() / 3 {
            positions.push(vec4(
                mesh.positions[3 * vtx],
                mesh.positions[3 * vtx + 1],
                mesh.positions[3 * vtx + 2],
                1.0,
            ));
        }

        geos.push(
            Geometry {
                positions,
                normals: vec![],
                uvs,
                indices,
            }
        )
    }

    geos
}