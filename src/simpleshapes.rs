use glam::{Vec4, vec4};
use crate::traits::Geometry;

/// Allows generation of simple shapes.
pub struct SimpleShapes {}

impl SimpleShapes {
    /// generates a triangle. No normals, uvs, or indices
    #[allow(dead_code)]
    pub fn generate_triangle() -> Geometry {
        Geometry {
            positions: vec![
                Vec4::new(0.0, -0.5, 1.0, 1.0),
                Vec4::new(0.5, 0.5, 1.0, 1.0),
                Vec4::new(-0.5, 0.5, 1.0, 1.0),
            ],
            normals: vec![],
            uvs: vec![],
            indices: vec![0,1,2],
        }
    }

    pub fn generate_fullscreen_triangle() -> Geometry {
        Geometry {
            positions: vec![
                Vec4::new(-1.0,1.0, 0.0, 1.0),
                Vec4::new(-1.0, -4.0, 0.0, 1.0),
                Vec4::new(4.0, 1.0, 0.0, 1.0),
            ],
            normals: vec![],
            uvs: vec![],
            indices: vec![],
        }
    }

    /// same as CubeGeo but differs in that the size is 1 unit
    /// No normals or uvs
    pub fn generate_cube() -> Geometry {
        Geometry {
            positions: vec![

                // front face
                vec4(-1.0, -1.0, 1.0, 1.0),
                vec4(1.0, -1.0, 1.0, 1.0),
                vec4(1.0, 1.0, 1.0, 1.0),
                vec4(-1.0, 1.0, 1.0, 1.0),

                // back face
                vec4(-1.0, -1.0, -1.0, 1.0),
                vec4(-1.0, 1.0, -1.0, 1.0),
                vec4(1.0, 1.0, -1.0, 1.0),
                vec4(1.0, -1.0, -1.0, 1.0),

                // top face
                vec4(-1.0, 1.0, -1.0, 1.0),
                vec4(-1.0, 1.0, 1.0, 1.0),
                vec4(1.0, 1.0, 1.0, 1.0),
                vec4(1.0, 1.0, -1.0, 1.0),

                // bottom face
                vec4(-1.0, -1.0, -1.0, 1.0),
                vec4(1.0, -1.0, -1.0, 1.0),
                vec4(1.0, -1.0, 1.0, 1.0),
                vec4(-1.0, -1.0, 1.0, 1.0),

                // right face
                vec4(1.0, -1.0, -1.0, 1.0),
                vec4(1.0, 1.0, -1.0, 1.0),
                vec4(1.0, 1.0, 1.0, 1.0),
                vec4(1.0, -1.0, 1.0, 1.0),


                // left face
                vec4(-1.0, -1.0, -1.0, 1.0),
                vec4(-1.0, -1.0, 1.0, 1.0),
                vec4(-1.0, 1.0, 1.0, 1.0),
                vec4(-1.0, 1.0, -1.0, 1.0),
            ],
            normals: vec![],
            uvs: vec![],
            indices: vec![
                0, 1, 2, 0, 2, 3,    // front
                4, 5, 6, 4, 6, 7,    // back
                8, 9, 10, 8, 10, 11,   // top
                12, 13, 14, 12, 14, 15,   // bottom
                16, 17, 18, 16, 18, 19,   // right
                20, 21, 22, 20, 22, 23,   // l
            ],
        }
    }
}