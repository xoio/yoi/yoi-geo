use glam::{Vec2, Vec4};

/// Defines the basic geometry object including vecs for positions,normals, uvs and indices.
pub struct Geometry {
    pub positions: Vec<Vec4>,
    pub normals: Vec<Vec4>,
    pub uvs: Vec<Vec2>,
    pub indices: Vec<u32>,
}

pub trait Geo {
    fn get_raw_geometry(&mut self) -> Geometry;
}